module.exports = observable => (parent = observable(), options = {}) => observable((publish, o) => {
  let subscription, source

  o.swap = newSource => {
    if(subscription) {
      subscription.unsubscribe()
    }
    source = newSource
    subscription = source.subscribe(publish)
    if(options.publishOnSwap) {
      publish(source())
    }
  }

  o.swap(parent)

  return () => subscription && subscription.unsubscribe()
}, options)
