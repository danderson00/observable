const observable = require('./observable')
const source = require('./source')
const helperObservables = require('./helperObservables')
const fromEmitter = require('./fromEmitter')
const merge = require('./merge')
const swappable = require('./swappable')
const utilities = require('./utilities')

module.exports = Object.assign(observable, {
  ...source(observable),
  fromEmitter: fromEmitter(observable),
  swappable: swappable(observable),
  ...helperObservables(observable),
  ...merge(observable),
  ...utilities.construct()
})
