module.exports = observable => ({
  proxy: (parent, options) => observable(
    publish => {
      const subscription = parent.subscribe(publish)
      return subscription.unsubscribe
    },
    { initialValue: parent(), parent, ...options }
  ),

  subject: options => observable(
    (publish, o) => {
      o.publish = publish
    },
    { source: true, ...options }
  )
})