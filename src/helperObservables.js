module.exports = observable => ({
  filterObservable: (parent, filter) => observable(publish => {
    const subscription = parent.subscribe(value => {
      if (filter(value)) {
        publish(value)
      }
    })
    return () => subscription.unsubscribe()
  }),

  mapObservable: (parent, transform) => observable(publish => {
    const subscription = parent.subscribe(value => {
      publish(transform(value))
    })
    return () => subscription.unsubscribe()
  })
})
