const construct = () => {
  const utilities = {
    uid: (current => () => ++current)(0),

    isObservable: o => o &&
      typeof o === 'function' &&
      typeof o.subscribe === 'function' &&
      typeof o.currentValue === 'function',

    unwrap: function (value) {
      if(!value) {
        return value
      } else if(isObservable(value)) {
        return unwrap(value())
      } else if(typeof value === 'object') {
        if(value.constructor === Array) {
          return value.map(unwrap)
        } else {
          return Object.keys(value).reduce(
            (result, name) => ({ ...result, [name]: unwrap(value[name]) }),
            {}
          )
        }
      } else {
        return value
      }
    }
  }
  const { isObservable, unwrap } = utilities
  return utilities
}

module.exports = construct()
module.exports.construct = construct