const { uid } = require('./utilities')

module.exports = function observable (publisher = () => {}, options = {}) {
  const { initialValue } = options

  let currentValue = initialValue
  let subscriptions = []

  const publish = value => {
    currentValue = value
    subscriptions.forEach(subscription => {
      if(subscription.onError) {
        try {
          subscription.handler(value)
        } catch (error) {
          subscription.onError(error)
        }
      } else {
        subscription.handler(value)
      }
    })
  }

  // create the observable from a function that returns the
  // current value and attach the various utility functions
  const observable = function Observable() { return currentValue }

  observable.subscribe = (handler, onError) => {
    const subscription = {
      handler,
      onError,
      token: uid(),
      unsubscribe: () => {
        subscriptions = subscriptions.filter(x => x !== subscription)
      }
    }
    subscriptions = [...subscriptions, subscription]
    return subscription
  }

  observable.unsubscribe = token => {
    subscriptions = subscriptions.filter(x => x.token !== token)
  }

  observable.currentValue = () => currentValue
  observable.disconnect = (...args) => typeof userDisconnect === 'function' && userDisconnect(...args)

  const userDisconnect = publisher(publish, observable, {
    getSubscriptions: () => subscriptions
  })

  return observable
}
