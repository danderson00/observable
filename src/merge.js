const { unwrap } = require('./utilities')

module.exports = observable => {
  const merge = (observables, update, initialValue) => observable(
    publish => {
      const subscriptions = observables.map(x => x.subscribe(
        value => update(publish, value)
      ))
      return () => {
        subscriptions.forEach(x => x.unsubscribe())
        observables.forEach(x => x.disconnect && x.disconnect())
      }
    },
    { initialValue }
  )

  return {
    merge: (...inputObservables) => merge(
      inputObservables,
      (publish, value) => publish(value)
    ),
    mergeHash: source => merge(
      Object.values(source),
      publish => publish(unwrap(source)),
      unwrap(source)
    ),
    mergeArray: (...inputObservables) => merge(
      inputObservables,
      publish => publish(unwrap(inputObservables)),
      unwrap(inputObservables)
    )
  }
}