module.exports = observable => (emitter, ...eventNames) => observable(
  publish => {
    const handlers = eventNames.map(eventName => {
      const listener = (...args) => publish({ topic: eventName, data: args[0], args })

      const addListenerFunction = (emitter.addEventListener || emitter.addListener || emitter.on)
      const removeListenerFunction = (emitter.removeEventListener || emitter.removeListener || emitter.off)

      return {
        add: () => addListenerFunction.call(emitter, eventName, listener),
        remove: () => removeListenerFunction && // silently ignores if emitter has no remove listener function
          removeListenerFunction.call(emitter, eventName, listener)
      }
    })
    handlers.forEach(x => x.add())
    return () => handlers.forEach(x => x.remove())
  },
  { source: true }
)
