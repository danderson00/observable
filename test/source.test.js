const observable = require('../src')

describe('proxy', () => {
  test("passes all messages through new observable", () => {
    let publish
    const handler = jest.fn()
    const o = observable(p => publish = p)
    observable.proxy(o).subscribe(handler)
    publish(1)
    publish('test')
    expect(handler.mock.calls.length).toEqual(2)
    expect(handler.mock.calls[0][0]).toEqual(1)
    expect(handler.mock.calls[1][0]).toEqual('test')
  })

  test("attaches disconnect function for detaching from input", () => {
    const handler = jest.fn()
    const o = observable.subject()
    const proxy = observable.proxy(o)
    proxy.subscribe(handler)
    o.publish(1)
    proxy.disconnect()
    o.publish('test')
    expect(handler.mock.calls.length).toEqual(1)
    expect(handler.mock.calls[0][0]).toEqual(1)
  })

  test("acquires initial value from input observable", () => {
    const o = observable.subject()
    o.publish(1)
    const proxy = observable.proxy(o)
    expect(proxy()).toBe(1)
  })
})

describe('subject', () => {
  test("can be triggered with exposed publish function", () => {
    const subject = observable.subject()
    const handler = jest.fn()
    subject.subscribe(handler)
    subject.publish(1)
    subject.publish('test')
    expect(handler.mock.calls.length).toEqual(2)
    expect(handler.mock.calls[0][0]).toEqual(1)
    expect(handler.mock.calls[1][0]).toEqual('test')
  })
})
