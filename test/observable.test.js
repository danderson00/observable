const observable = require('../src/observable')
const utility = require('../src/source')(observable)

test("subscription handlers are called", () => {
  let publish = () => {}
  const handlers = [jest.fn(), jest.fn()]
  const o = observable(p => publish = p)
  o.subscribe(handlers[0])
  o.subscribe(handlers[1])
  publish(1)
  publish('test')
  expect(handlers.map(x => x.mock.calls.length)).toEqual([2, 2])
  expect(handlers.map(x => x.mock.calls[0][0])).toEqual([1, 1])
  expect(handlers.map(x => x.mock.calls[1][0])).toEqual(['test', 'test'])
})

test("subscriptions can be unsubscribed", () => {
  let publish = () => {}
  const handlers = [jest.fn(), jest.fn()]
  const o = observable(p => publish = p)
  const subscriptions = [
    o.subscribe(handlers[0]),
    o.subscribe(handlers[1])
  ]
  publish(1)
  subscriptions[0].unsubscribe()
  publish('test')
  expect(handlers.map(x => x.mock.calls.length)).toEqual([1, 2])
  expect(handlers.map(x => x.mock.calls[0][0])).toEqual([1, 1])
  expect(handlers[1].mock.calls[1][0]).toEqual('test')
})

test("subscriptions can be unsubscribed using tokens", () => {
  let publish = () => {}
  const handlers = [jest.fn(), jest.fn()]
  const o = observable(p => publish = p)
  const subscriptions = [
    o.subscribe(handlers[0]),
    o.subscribe(handlers[1])
  ]
  publish(1)
  o.unsubscribe(subscriptions[0].token)
  publish('test')
  expect(handlers.map(x => x.mock.calls.length)).toEqual([1, 2])
  expect(handlers.map(x => x.mock.calls[0][0])).toEqual([1, 1])
  expect(handlers[1].mock.calls[1][0]).toEqual('test')
})

test("current value of observable can be accessed by executing observable or currentValue", () => {
  const subject = utility.subject()
  const o = utility.proxy(subject)
  subject.publish(1)
  expect(o()).toBe(1)
  expect(o.currentValue()).toBe(1)
  subject.publish('test')
  expect(o()).toBe('test')
  expect(o.currentValue()).toBe('test')
})

test("initialValue option sets initial currentValue of observable", () => {
  const o = observable(() => {}, { initialValue: 10 })
  expect(o()).toBe(10)
  const subject = utility.subject({ initialValue: 20 })
  expect(subject()).toBe(20)
})

test("error bubbles to publish if subscriber without onError throws", () => {
  const source = utility.subject()
  source.subscribe(() => { throw new Error('test') })
  expect(() => source.publish({})).toThrowError('test')
})

test("onError is called if subscriber throws", () => {
  const source = utility.subject()
  const onError = jest.fn()
  source.subscribe(() => { throw new Error('test') }, onError)
  source.publish({})
  expect(onError.mock.calls).toEqual([[new Error('test')]])
})

test("result of observable constructor is disconnect function", () => {
  const spy = jest.fn()
  const source = observable(() => spy)
  source.disconnect()
  expect(spy.mock.calls).toEqual([[]])
})