const observable = require('../src')

describe('merge', () => {
  const setup = () => {
    const source1 = observable.subject()
    source1.disconnect = jest.fn()
    const source2 = observable.subject()
    const merged = observable.merge(source1, source2)
    const handler = jest.fn()
    merged.subscribe(handler)
    return { source1, source2, handler, merged }
  }

  test("passes messages from all provided observables to new observable", () => {
    const { source1, source2, handler } = setup()

    source1.publish(1)
    source1.publish(2)
    source2.publish(3)
    source1.publish(4)

    expect(handler.mock.calls).toEqual([[1], [2], [3], [4]])
  })

  test("unsubscribes when disconnect is called", () => {
    const { source1, source2, handler, merged } = setup()

    source1.publish(1)
    source1.publish(2)
    merged.disconnect()
    source2.publish(3)
    source1.publish(4)

    expect(handler.mock.calls).toEqual([[1], [2]])
  })

  test("relays disconnect to source observables", () => {
    const { source1, merged } = setup()
    merged.disconnect()
    expect(source1.disconnect.mock.calls.length).toBe(1)
  })
})

describe("mergeHash", () => {
  const setup = () => {
    const source1 = observable.subject()
    source1.disconnect = jest.fn()
    const source2 = observable.subject()
    const merged = observable.mergeHash({ source1, source2 })
    const handler = jest.fn()
    merged.subscribe(handler)
    return { source1, source2, handler, merged }
  }

  test("sets initial value", () => {
    const { merged } = setup()
    expect(merged()).toEqual({ source1: undefined, source2: undefined })
  })

  test("publishes unwrapped merged observable when any source observable pulses", () => {
    const { source1, source2, handler } = setup()

    source1.publish(1)
    source1.publish(2)
    source2.publish(3)
    source1.publish(4)

    expect(handler.mock.calls).toEqual([
      [{ source1: 1, source2: undefined }],
      [{ source1: 2, source2: undefined }],
      [{ source1: 2, source2: 3 }],
      [{ source1: 4, source2: 3 }]
    ])
  })

  test("unsubscribes when disconnect is called", () => {
    const { source1, source2, handler, merged } = setup()

    source1.publish(1)
    source1.publish(2)
    merged.disconnect()
    source2.publish(3)
    source1.publish(4)

    expect(handler.mock.calls).toEqual([
      [{ source1: 1, source2: undefined }],
      [{ source1: 2, source2: undefined }]
    ])
  })

  test("relays disconnect to source observables", () => {
    const { source1, merged } = setup()
    merged.disconnect()
    expect(source1.disconnect.mock.calls.length).toBe(1)
  })
})

describe("mergeArray", () => {
  const setup = () => {
    const source1 = observable.subject({ initialValue: 1 })
    source1.disconnect = jest.fn()
    const source2 = observable.subject()
    const merged = observable.mergeArray(source1, source2)
    const handler = jest.fn()
    merged.subscribe(handler)
    return { source1, source2, handler, merged }
  }

  test("sets initial value", () => {
    const { merged } = setup()
    expect(merged()).toEqual([1, undefined])
  })

  test("publishes unwrapped merged observable when any source observable pulses", () => {
    const { source1, source2, handler } = setup()

    source1.publish(1)
    source1.publish(2)
    source2.publish(3)
    source1.publish(4)

    expect(handler.mock.calls).toEqual([
      [[1, undefined]],
      [[2, undefined]],
      [[2, 3]],
      [[4, 3]]
    ])
  })

  test("unsubscribes when disconnect is called", () => {
    const { source1, source2, handler, merged } = setup()

    source1.publish(1)
    source1.publish(2)
    merged.disconnect()
    source2.publish(3)
    source1.publish(4)

    expect(handler.mock.calls).toEqual([
      [[1, undefined]],
      [[2, undefined]]
    ])
  })

  test("relays disconnect to source observables", () => {
    const { source1, merged } = setup()
    merged.disconnect()
    expect(source1.disconnect.mock.calls.length).toBe(1)
  })
})