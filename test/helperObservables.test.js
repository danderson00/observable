const { filterObservable, mapObservable, subject } = require('../src')

test("filterObservable filters messages based on predicate function", () => {
  const source = subject()
  const spy = jest.fn()
  const o = filterObservable(source, x => x.value > 2)
  o.subscribe(spy)
  source.publish({ value: 1 })
  source.publish({ value: 2 })
  source.publish({ value: 3 })
  expect(spy.mock.calls).toEqual([
    [{ value: 3 }]
  ])
})

test("filterObservable unsubscribes from parent when disconnected", () => {
  const source = subject()
  const spy = jest.fn()
  const o = filterObservable(source, x => x.value > 2)
  o.subscribe(spy)
  source.publish({ value: 3 })
  o.disconnect()
  source.publish({ value: 4 })
  expect(spy.mock.calls).toEqual([
    [{ value: 3 }]
  ])
})

test("mapObservable maps messages values based on transform function", () => {
  const source = subject()
  const spy = jest.fn()
  const o = mapObservable(source, x => ({ value: x * 2 }))
  o.subscribe(spy)
  source.publish(1)
  source.publish(2)
  expect(spy.mock.calls).toEqual([
    [{ value: 2 }],
    [{ value: 4 }]
  ])
})

test("mapObservable unsubscribes from parent when disconnected", () => {
  const source = subject()
  const spy = jest.fn()
  const o = mapObservable(source, x => ({ value: x * 2 }))
  o.subscribe(spy)
  source.publish(1)
  o.disconnect()
  source.publish(2)
  expect(spy.mock.calls).toEqual([
    [{ value: 2 }]
  ])
})