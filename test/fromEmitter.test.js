const observable = require('../src')
const EventEmitter = require('events')

test("fromEmitter publishes events of given type from EventEmitter", () => {
  const emitter = new EventEmitter()
  const o = observable.fromEmitter(emitter, 'message')
  expect(emitter.listenerCount('message')).toBe(1)
  emitter.emit('close', 'testData')
  expect(o()).toBeUndefined()
  emitter.emit('message', 'test')
  expect(o()).toEqual({ topic: 'message', data: 'test', args: ['test'] })
})

test("fromEmitter publishes multiple wrapped events from EventEmitter if multiple event names are specified", () => {
  const emitter = new EventEmitter()
  const o = observable.fromEmitter(emitter, 'message', 'data')
  expect(emitter.listenerCount('message')).toBe(1)
  expect(emitter.listenerCount('data')).toBe(1)
  emitter.emit('close', 'test')
  expect(o()).toBeUndefined()
  emitter.emit('message', 'test')
  expect(o()).toEqual({ topic: 'message', data: 'test', args: ['test'] })
  emitter.emit('data', 'testData')
  expect(o()).toEqual({ topic: 'data', data: 'testData', args: ['testData'] })
})

test("fromEmitter disconnects from EventEmitter when disconnect is called", () => {
  const emitter = new EventEmitter()
  const o = observable.fromEmitter(emitter, 'message', 'close')
  o.disconnect()
  expect(emitter.listenerCount('message')).toBe(0)
  expect(emitter.listenerCount('close')).toBe(0)
  emitter.emit('message', 'test')
  emitter.emit('close', 'test')
  expect(o()).toBeUndefined()
})

