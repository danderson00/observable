import { Observable } from './observable'
import { Proxy } from './source'

/**
 *
 * @param inputObservables
 */
export function merge(...inputObservables: Observable[]): Proxy

/**
 *
 * @param source
 */
export function mergeHash(source: object): Proxy

/**
 *
 * @param inputObservables
 */
export function mergeArray(...inputObservables: Observable[]): Proxy
