import { Publish } from './delegates'
import { Observable, ObservableOptions } from './observable'

/**
 * Create a new instance of a {@link Subject} observable.
 * @param options
 */
export function subject(options?: ObservableOptions): Subject

/**
 * Create a new instance of a {@link Proxy} observable.
 * @param parentObservable
 * @param options
 */
export function proxy(parentObservable: Observable, options?: ObservableOptions): Proxy

/**
 * A special kind of {@link Observable} that relays messages from a source observable.
 */
type Proxy = Observable & {
  /**
   * Disconnect the observable from its source.
   */
  disconnect: () => void
}

/**
 * A special kind of {@link Observable} has an extra property called `publish` that allows values to be emitted from the
 * observable from an external source.
 */
type Subject = Observable & {
  /**
   * Emit a value from the observable
   */
  publish: Publish
}
