import { Proxy } from './source'

/**
 *
 * @param emitter
 * @param eventNames
 */
export function fromEmitter(emitter: any, ...eventNames: string[]): Proxy
