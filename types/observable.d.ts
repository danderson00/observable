import { ErrorHandler, MessageHandler, Publish } from './delegates'

/**
 * Create a new instance of an observable
 * @param publisher - A function that encapsulates the definition of an observable
 * @param options
 * @category Factory
 */
export function observable(publisher?: Publisher, options?: ObservableOptions): Observable

/**
 * An `Observable` represents a stream of messages. The object itself is a function that can be executed to retrieve the
 * most recent stream value.
 */
export type Observable = {
  /**
   * Retrieve the most recently emitted value
   */
  (): any,
  /**
   * Create a subscription to the {@link Observable}, passing a {@link MessageHandler} that is executed every time a
   * value is emitted.
   * @param MessageHandler
   */
  subscribe: (handler: MessageHandler) => Subscription,
  /**
   * Remove a specific subscription from the {@link Observable} that is associated to the provided token.
   * @param token
   */
  unsubscribe: (token: string) => void,
  /**
   * Retrieve the most recently emitted value
   */
  currentValue: () => any
}

/**
 * A function that encapsulates the definition of an observable. The observable can be made to emit values by calling
 * the passed {@link Publish} function. The returned {@link Observable} instance is passed as the second parameter and
 * can be directly modified in the `Publisher` function.
 * @param publish - Causes the observable to emit values
 * @param observable - The returned observable instance
 */
export type Publisher = (publish: Publish, observable: Observable) => void

export interface Subscription {
  handler: MessageHandler,
  onError: ErrorHandler,
  token: string,
  unsubscribe: () => {}
}

export interface ObservableOptions {
  initialValue?: any
}
