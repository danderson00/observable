/**
 * Handle messages emitted by the observable
 * @param message - The message that was emitted
 */
export type MessageHandler = (message: any) => void

/**
 * Handle errors raised by the observable
 * @param error - The error that occurred
 */
export type ErrorHandler = (error: any) => void

/**
 * Cause the observable to emit a value
 * @param message - The value to emit
 */
export type Publish = (message: any) => void