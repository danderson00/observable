import { Observable, ObservableOptions } from './observable'
import { Proxy } from './source'

export function swappable(inputObservable: Observable, options?: SwappableOptions): SwappableObservable

/**
 * A special kind of {@link Proxy} that can have the {@link Observable} that it is proxying swapped.
 */
export type SwappableObservable = Proxy & {
  /**
   * Swap the {@link Observable} that this observable is proxying
   * @param newInputObservable
   */
  swap: (newInputObservable: Observable) => void
}

export interface SwappableOptions extends ObservableOptions {
  publishOnSwap?: boolean
}
