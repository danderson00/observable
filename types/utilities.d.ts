export function unwrap(source: any): any
export function isObservable(source: any): boolean
export function uid(): number
